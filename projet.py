from typing import List, Tuple
from typing import Dict
from typing import Optional
from typing import Iterable
import requests
from pprint import pprint
import itertools
import subprocess
import test

import crocomine_client

server = "http://localhost:8000"
group = "Groupe 60"
members = "Marine et Nicolas"
password = ":("
croco = crocomine_client.CrocomineClient(server, group, members, password)

# compter le nb de case avec des infos  pour arreter le parcours de la grille dès qu'on les a trouvé
# etat d'une case
etat = {
    1 : "Tigre",
    2 : "Requin",
    3 : "Croco",
    4 : "Libre"
}

# Dictionnaire avec le nombre de tigre de requins et de crocodile

largeur = 0
longueur = 0

# Grille avec les coordonnées de 0 à longueur, de 0 à largeur, int pour l'état de la cellule
grille =  []
#tailleTotale = (longueur + 2) * (largeur + 2)

indice3BV = 0

nbMers = 0
nbMersDecouvertes = 0
nbTerres = 0
nbTerresDecouvertes = 0

nbTigres = 0
nbTigresDecouverts = 0
nbRequins = 0
nbRequinsDecouverts = 0
nbCrocos = 0
nbCrocosDecouverts = 0

correspondanceLitteraux = {int : Tuple} # dictionnaire qui associe un littéral à un couple de coordonnées
correspondanceCoord = {Tuple : int} #dictionnaire qui fait coorespondre des coordonnées sous forme de tuple (int, int) à un littéral int

nbEtats = 4 # tigre requin crocodile rien

bordure = {Tuple : int} # liste des états inconnus bordant des états connus avec le nombre de voisins connus 
depart = Tuple
#
# Lorsque qu'on reçoit des infos sur des cases, on regarde si elles sont dans la liste de bordure.
# Si oui on les retire de bordure et on ajoute les voisins inconnus de cette case si elle en a
# Si non on ajoute les voisins inconnus de cette coordonnée dans la liste
# il faut mettre à jour la grille en parallèle
# bordure servirait à éviter de reparcourir toute la grille à chaque fois qu'on a un nouvel appel à l'api

clauses = [List[int]]
nbLitteraux = 1


#voisins=[grille[i-1][j-1], grille[i-1][j], grille[i-1][j+1], grille[i][j-1], grille[i][j+1], grille[i+1][j-1], grille[i+1][j], grille[i+1][j+1]]

def at_least_one(vars: List[int])  :
    cpy = vars[:]
    return cpy

def unique(vars: List[int]) :
    clauses = []
    clauses.append(vars)
    result = itertools.combinations(vars, 2) #retourne une liste des différentes combinaisons et donc de chaque clause
    for i in result: #la clause
        clause = []
        for j in i: #le littéral dans la clause
            litteral = -j
            clause.append(litteral)
        clauses.append(clause)
    return clauses

# renvoie une chaine de caractère
def clauses_to_dimacs(vars : List[List]) :
    dimacs = 'p cnf ' + str(nbLitteraux - 1) + ' '+ str(len(vars)) +'\n'
    for clause in vars:
        for litteral in clause:
            dimacs += str(litteral) + ' '
        dimacs +='0\n'
    return dimacs

def clauses_to_dimacs2(vars : List[List], string) :
    dimacs = 'p cnf ' + str(nbLitteraux - 1) + ' '+ str(len(vars) + 1) +'\n'
    for clause in vars:
        for litteral in clause:
            dimacs += str(litteral) + ' '
        dimacs +='0\n'
    dimacs += string + "\n"
    return dimacs

def initialisation() :
    #déclaration nécessaire pour utiliser une variable globale qu'on modfie
    global grille
    global longueur
    global largeur
    global nbCrocos
    global nbTigres
    global nbRequins
    global nbTerres
    global nbMers
    global depart
    
    status, msg, grid_infos = croco.new_grid() #appel de la fonction d'API
    if(status == 'Err') : 
        return status
    print(status, msg)
    pprint(grid_infos)

    longueur = grid_infos['m']
    largeur = grid_infos['n']
    statut = status
    message = msg
    nbMers = grid_infos['sea_count']
    nbTerres = grid_infos['land_count']
    nbCrocos = grid_infos['croco_count']
    nbRequins = grid_infos['shark_count']
    nbTigres = grid_infos['tiger_count']
    indice3BV = grid_infos['3BV']
    infos = grid_infos['infos']
    depart = (grid_infos['start'][0], grid_infos['start'][1])
    #creation de la grille
    del grille[:]
    grille = []
    for i in range(longueur):
        line=[]
        for j in range(largeur):
            case = {
                'nbTigres'  : -1,
                'nbRequins' : -1,
                'nbCrocos'   : -1,
                'field' : '?',
                'etat'      : -1
            }
            line.append(case)
        grille.append(line)

    #ajout des infos de depart
    for info in infos :
        ajoutInfoGrille(info)
    return status

# cas plantage = une celulle n'est entourée que d'animaux, les animaux ne osnt pas dans la bordure = erreur
# si bordure vide et grille pas finie
def miseAJourBordure(i : int, j : int) : 
    global bordure
    global grille
    voisins = voisinage(i, j)
    nbVoisinsInconnus = 0
    nbVoisinsConnus = 0
    # on récupère la liste de voisin de la celulle 
    for voisin in voisins : 
        x = voisin[0]
        y = voisin[1]
        # on compte le nombre de voisins inconnus 
        if(grille[x][y]['etat'] == -1) :
            nbVoisinsInconnus += 1
        # si le voisin est connu 
        elif(grille[x][y]['etat'] != -1) :
            nbVoisinsConnus += 1
            if((x,y) in bordure) : # si le voisin connu est dans la bordure 
                bordure[(x,y)] = bordure[(x,y)] - 1 # on diminue le nb de voisins inconnus de ce voisin connu 
                if(bordure[(x,y)] == 0) : # si ce voisin n'a plus de voisins inconnus, on le supprime de la bordure
                    bordure.pop((x,y))
    # on récupère le nombre d'animaux dans la proximité de la case
    # s'il n'y a pas d'animaux c'est que la case n'est pas libre, elle ne donne pas d'info et n'a pas à figurer dans la bordure. 
    # techniquement il n'y a que les cases libres mais pour être sûr il vaut mieux tester sur le nb d'animaux, au cas où le prof change un truc
    nbAnimaux = grille[i][j]['nbTigres'] + grille[i][j]['nbRequins'] + grille[i][j]['nbCrocos']
    if((i,j) not in bordure and nbAnimaux >= 0 and nbVoisinsInconnus > 0) : 
        bordure[(i,j)] = nbVoisinsInconnus
        
def ajoutInfoGrille(info : Dict) :
    global grille
    global nbTerresDecouvertes
    global nbMersDecouvertes
    i = info['pos'][0]
    j = info['pos'][1]
    # si le prox count est donné et qu'on avait pas d'infos pour cette cellule
    if('prox_count' in info and len(info['prox_count']) > 0 and grille[i][j]['nbTigres'] == -1 and grille[i][j]['nbRequins'] == -1 and grille[i][j]['nbCrocos'] == -1 ) : 
        grille[i][j]['nbTigres'] = info['prox_count'][0]
        grille[i][j]['nbRequins'] = info['prox_count'][1]
        grille[i][j]['nbCrocos'] = info['prox_count'][2]
        verifierVoisins(i, j) # on vérifie qu'on n'a pas déjà trouvé tous les animaux du voisinage
        # si il y a prox count même si c'est 0 0 0 c'est que c'est case connue 
        grille[i][j]['etat'] = 4
        miseAJourBordure(i, j) # la case sera ajoutée dans la bordure si elle n'a pas tous ces voisins de connus et que la case à un prox_count non nul
    if('field' in info and len(info['field']) > 0) : # si le type de terrain est donné 
        grille[i][j]['field'] = info['field'] # on stocke le type de terrain si on l'a 
    if (grille[i][j]['field'] == 'land') :
        nbTerresDecouvertes += 1
    elif (grille[i][j]['field'] == 'sea') :
        nbMersDecouvertes += 1

def recupLitteralTigre(coordonnees : tuple): #recupere le litteral Tigre associé aux coordonnee d'une case
    global nbLitteraux
    global correspondanceCoord
    global correspondanceLitteraux
    if(coordonnees not in correspondanceCoord) :
        correspondanceCoord[coordonnees] = nbLitteraux
        correspondanceLitteraux[nbLitteraux] = coordonnees
        correspondanceLitteraux[nbLitteraux+1] = coordonnees
        correspondanceLitteraux[nbLitteraux+2] = coordonnees
        correspondanceLitteraux[nbLitteraux+3] = coordonnees
        nbLitteraux += nbEtats
    return correspondanceCoord.get(coordonnees)

def recupLitteralRequin(coordonnees : tuple):
    global nbLitteraux
    global correspondanceCoord
    global correspondanceLitteraux
    if(coordonnees not in correspondanceCoord) :
        correspondanceCoord[coordonnees] = nbLitteraux
        correspondanceLitteraux[nbLitteraux] = coordonnees
        correspondanceLitteraux[nbLitteraux+1] = coordonnees
        correspondanceLitteraux[nbLitteraux+2] = coordonnees
        correspondanceLitteraux[nbLitteraux+3] = coordonnees
        nbLitteraux += nbEtats
    return correspondanceCoord.get(coordonnees) + 1

def recupLitteralCroco(coordonnees : tuple):
    global nbLitteraux
    global correspondanceCoord
    global correspondanceLitteraux
    if(coordonnees not in correspondanceCoord) :
        correspondanceCoord[coordonnees] = nbLitteraux
        correspondanceLitteraux[nbLitteraux] = coordonnees
        correspondanceLitteraux[nbLitteraux+1] = coordonnees
        correspondanceLitteraux[nbLitteraux+2] = coordonnees
        correspondanceLitteraux[nbLitteraux+3] = coordonnees
        nbLitteraux += nbEtats
    return correspondanceCoord.get(coordonnees) + 2

def recupLitteralLibre(coordonnees : tuple):
    global nbLitteraux
    global correspondanceCoord
    global correspondanceLitteraux
    if(coordonnees not in correspondanceCoord) :
        correspondanceCoord[coordonnees] = nbLitteraux
        correspondanceLitteraux[nbLitteraux] = coordonnees
        correspondanceLitteraux[nbLitteraux+1] = coordonnees
        correspondanceLitteraux[nbLitteraux+2] = coordonnees
        correspondanceLitteraux[nbLitteraux+3] = coordonnees
        nbLitteraux += nbEtats
    return correspondanceCoord.get(coordonnees) + 3

# liste de voisins inconnus en paramètre
def listTigre(voisins:list[tuple]):#renvoie la liste des litteraux tigres des voisins
    listTigre= []
    for each in voisins:
        Lit=recupLitteralTigre(each)
        listTigre.append(Lit)
    return listTigre

# liste de voisins inconnus en paramètre
def listRequin(voisins:list[tuple]):#renvoie la liste des litteraux requins des voisins
    listRequin= []
    for each in voisins:
        Lit=recupLitteralRequin(each)
        listRequin.append(Lit)
    return listRequin

# liste de voisins inconnus en paramètre
def listCroco(voisins:list[tuple]):#renvoie la liste des litteraux crocodiles des voisins
    listCroco= []
    for each in voisins:
        Lit=recupLitteralCroco(each)
        listCroco.append(Lit)
    return listCroco

# liste de voisins inconnus en paramètre
def listLibre(voisins:list[tuple]):#renvoie la liste des litteraux Libre des voisins
    listLibre= []
    for each in voisins:
        Lit=recupLitteralCroco(each)
        listLibre.append(Lit)
    return listLibre

def recupLitteraux(coordonnees : tuple): #renvoi la liste de tt les litteraux d'une case
    listTigre_listCroco_listRequin = [recupLitteralCroco(coordonnees), recupLitteralLibre(coordonnees), recupLitteralRequin(coordonnees), recupLitteralTigre(coordonnees)]
    return listTigre_listCroco_listRequin

#cellulle connue
def AjoutContraintesAnimalDansCNF(i : int, j: int, voisinsInconnus : List[Tuple]):
    R = grille[i][j]['nbRequins'] #nb de requins autour
    T = grille[i][j]['nbTigres'] #nb de tigres autour
    C = grille[i][j]['nbCrocos'] #nb de tigres autour
    CNF = []
    if R > 0 and  T > 0 and  C > 0:
        listlitterauxTigres = listTigre(voisinsInconnus) 
        contrainte = ajoutDansCNF(listlitterauxTigres, T)
        CNF.extend(contrainte)
        
        listlitterauxRequins = listRequin(voisinsInconnus)
        contrainte = (ajoutDansCNF(listlitterauxRequins, R))
        CNF.extend(contrainte)

        listlitterauxCrocos = listCroco(voisinsInconnus) 
        contrainte = (ajoutDansCNF(listlitterauxCrocos, C))
        CNF.extend(contrainte)

    elif R > 0 and T > 0 and C == 0:
        listlitterauxTigres = listTigre(voisinsInconnus) 
        contrainte = (ajoutDansCNF(listlitterauxTigres, T))
        CNF.extend(contrainte)
        
        listlitterauxRequins = listRequin(voisinsInconnus)
        contrainte = (ajoutDansCNF(listlitterauxRequins, R))
        CNF.extend(contrainte)

    elif R > 0 and C > 0: 
        listlitterauxRequins = listRequin(voisinsInconnus)
        contrainte = (ajoutDansCNF(listlitterauxRequins, R))
        CNF.extend(contrainte)

        listlitterauxCrocos = listCroco(voisinsInconnus) 
        contrainte = (ajoutDansCNF(listlitterauxCrocos, C))
        CNF.extend(contrainte)

    elif T > 0 and C > 0:
        listlitterauxTigres = listTigre(voisinsInconnus) 
        contrainte = (ajoutDansCNF(listlitterauxTigres, T))
        CNF.extend(contrainte)

        listlitterauxCrocos = listCroco(voisinsInconnus) 
        contrainte = (ajoutDansCNF(listlitterauxCrocos, C))
        CNF.extend(contrainte)

    elif T > 0:
        listlitterauxTigres = listTigre(voisinsInconnus)
        contrainte = ajoutDansCNF(listlitterauxTigres, T) #ajout des contraintes a notre base de clauses
        CNF.extend(contrainte)
    elif R > 0:
        listlitterauxRequins = listRequin(voisinsInconnus)
        contrainte = ajoutDansCNF(listlitterauxRequins, R) #ajout des contraintes a notre base de clauses
        CNF.extend(contrainte)

    elif C > 0:
        listlitterauxCrocos = listCroco(voisinsInconnus)
        contrainte = ajoutDansCNF(listlitterauxCrocos, C) #ajout des contraintes a notre base de clauses
        CNF.extend(contrainte)

    return CNF

def ajoutDansCNF(litteraux:list[int],k:int):
    n=len(litteraux)
    CNF = []
    result = itertools.combinations(litteraux, k+1) #clauses de litteraux negatifs
    for i in result: #la clause
        clause = []
        for j in i: #le littéral dans la clause
            litteral = -j
            clause.append(litteral)
        CNF.append(clause)
    result2 = itertools.combinations(litteraux, n-(k-1)) #clauses de litteraux positifs
    for each_clause in result2:
        CNF.append(list(each_clause))
    return CNF


def decouvrir(i : int, j : int) :
    global grille
    Status, Msg, Infos = croco.discover(i, j)
    statut = Status
    message = Msg
    print(statut)
    print(Msg)
    pprint(Infos)
    grille[i][j]['etat'] = 4
    for info in Infos :
        # faire la différence entre les cases libres avec prox_count = à zéro et case avec un terrain 
        ajoutInfoGrille(info)
    return statut

def verifierVoisins(i : int, j : int) :
    global grille  
    if(grille[i][j]['nbTigres'] > 0 or grille[i][j]['nbRequins'] > 0 or grille[i][j]['nbCrocos'] > 0) :
        voisins = voisinage(i, j)
        for voisin in voisins :
            x = voisin[0]
            y = voisin[1]
            if(grille[x][y]['etat'] == 1) : # Tigre
                grille[i][j]['nbTigres'] -= 1
            elif(grille[x][y]['etat'] == 2) : # Requin
                grille[i][j]['nbRequins'] -= 1
            elif(grille[x][y]['etat'] == 3) : #Crocodile
                grille[i][j]['nbCrocos'] -= 1
# faire la vérif du message/statut
# avant d'appeler cette fonction, on met T, S ou C dans etat
# et du coup animal ce sera grille[i][j][etat]
def devinerAnimal(i : int, j : int, animal : str) :
    # est-ce qu'on a des infos sur le voisinage d'une cellule avec un animal ?
    global nbTigresDecouverts
    global nbRequinsDecouverts
    global nbCrocosDecouverts
    Status, Msg, Infos = croco.guess(i, j, animal)
    statut = Status
    message = Msg
    print(statut) 
    print(message)
    pprint(Infos)
    if(animal == 'T') :
        grille[i][j]['etat'] = 1
        nbTigresDecouverts += 1
        voisins = voisinage(i, j)
        for voisin in voisins :
            x = voisin[0]
            y = voisin[1]
            #mettre à jour les données de ceux dans la bordure 
            if((x, y) in bordure and grille[x][y]['nbTigres'] > 0) :
                grille[x][y]['nbTigres'] -= 1

    elif(animal == 'S') :
        grille[i][j]['etat'] = 2
        nbRequinsDecouverts += 1
        voisins = voisinage(i, j)
        for voisin in voisins :
            x = voisin[0]
            y = voisin[1]
            if((x, y) in bordure and grille[x][y]['nbRequins'] > 0) :
                grille[x][y]['nbRequins'] -= 1

    elif(animal == 'C') :
        grille[i][j]['etat'] = 3
        nbRequinsDecouverts += 1
        voisins = voisinage(i, j)
        for voisin in voisins :
            x = voisin[0]
            y = voisin[1]
            if((x, y) in bordure and grille[x][y]['nbCrocos'] > 0) :
                grille[x][y]['nbCrocos'] -= 1
    miseAJourBordure(i,j) # va permettre d'enlever les voisins connus
        # mettre à jour le voisinage avec -1
    #if('infos' in resultat and len(resultat['infos']) > 0) : # si des infos sont données
    for info in Infos :
          ajoutInfoGrille(info)
    return statut

def voisinage(i : int, j : int) :
    if(i > 0 and i < (longueur-1) and j > 0 and j < (largeur-1)) :
        return [(i-1, j-1), (i-1, j), (i-1, j+1), (i, j-1), (i, j+1), (i+1,j-1), (i+1, j), (i+1, j+1)]
    elif(i == (longueur-1) and j > 0 and j < (largeur-1)) :
        return [(i-1, j-1), (i-1, j), (i-1, j+1), (i, j-1), (i, j+1)]
    elif(i == 0 and j > 0 and j < (largeur-1)) :
        return [(i, j-1), (i, j+1), (i+1, j-1), (i+1, j), (i+1, j+1)]
    elif(i > 0 and i < (longueur-1) and j == (largeur-1)) :
        return [(i-1, j-1), (i-1, j), (i, j-1), (i+1, j-1), (i+1, j)]
    elif(i > 0 and i < (longueur-1) and j == 0) :
        return [(i-1, j), (i-1, j+1), (i,j+1), (i+1, j), (i+1, j+1)]
    elif(i == (longueur-1) and j == 0) :
        return [(i-1, j), (i-1, j+1), (i,j+1)]
    elif(i == 0 and j == (largeur-1)) :
        return [(i, j-1), (i+1, j-1), (i+1, j)]
    elif(i == (longueur-1) and j == (largeur-1)) :
        return [(i-1, j-1), (i-1, j), (i, j-1)]
    elif(i == 0 and j == 0) :
        return [(i, j+1), (i+1, j), (i+1, j+1)]

# on parcourt toute la grille
def constructionClauses() : #revoir le nom de cette fonction
    # on parcourt la grille à la recherche des bords
    global nbLitteraux
    global clauses 
    clauses = []
    global correspondanceLitteraux
    global correspondanceCoord

    correspondanceLitteraux.clear() # nouvelle recherche on vide la correspondance
    correspondanceCoord.clear()

    nbLitteraux = 1 # on remet à 1
    voisinsInconnus = set()
    # on itère sur les clès de bordure   
    for cellule in bordure : 
        i = cellule[0]
        j = cellule[1]
        voisinsInconnusCellule = set()
        voisins = voisinage(i, j)
        for voisin in voisins :
            x = voisin[0]
            y = voisin[1]
            if(grille[x][y]['etat'] == -1) : 
                # si on n'a pas déjà vu ce voisin inconnu
                if ((x,y) not in voisinsInconnus) :
                    voisinsInconnus.add((x, y))
                    contraintes = unique([recupLitteralCroco(voisin), recupLitteralRequin(voisin), recupLitteralTigre(voisin), recupLitteralLibre(voisin)])
                    clauses.extend(contraintes)
                if ((x,y) not in voisinsInconnusCellule) :
                    voisinsInconnusCellule.add((x, y))
                contraintes =  AjoutContraintesEtatCelulleDansCNF((i, j), (x, y)) 
                clauses.extend(contraintes)
        contraintes = AjoutContraintesAnimalDansCNF(i, j, voisinsInconnusCellule)
        clauses.extend(contraintes)
    return clauses
    
# inconnu : coordonnées de la celulle inconnue
# connu : coordonnées de la celulle connue 
def AjoutContraintesEtatCelulleDansCNF(connu : tuple, inconnu : tuple) :
    contraintes = []
    animaux = []
    i = connu[0]
    j = connu[1]

    x = inconnu[0]
    y = inconnu[1]

    if(grille[i][j]['nbTigres'] > 0 and grille[x][y]['field'] == 'land' ) :
        animaux.append(recupLitteralTigre(inconnu))
    else :
        contraintes.append([-recupLitteralTigre(inconnu)])

    if(grille[i][j]['nbRequins'] > 0 and grille[x][y]['field'] == 'sea') :
        animaux.append(recupLitteralRequin(inconnu))
    else :
        contraintes.append([-recupLitteralRequin(inconnu)])

    if(grille[i][j]['nbCrocos'] > 0) :
        animaux.append(recupLitteralCroco(inconnu))
    else :
        contraintes.append([-recupLitteralCroco(inconnu)])

    if(len(animaux)>0) :
        animaux.append(recupLitteralLibre(inconnu))
        contraintes.append(animaux)
    elif(len(animaux) == 0) : # si ça ne peut pas être un tigre ni un requin ni un croco alors c'est forcément libre 
        contraintes.append([recupLitteralLibre(inconnu)])
    # contrainte qui précise que ça ne peut être qu'un seul de ces états 
    return contraintes


# retourne un tuple 
def litteralACoordonnees(litteral : int) :
    return correspondanceLitteraux[litteral]


def appelGophersat() :
    #os.system('gophersat-1.1.6.exe "input_projet.cnf" "output.txt"')
    p = subprocess.call('gophersat-1.1.6.exe input_projet.cnf')
    #subprocess.run(['gophersat-1.1.6.exe', 'input_projet.cnf'])

def ecrireClausesDansFichier(dimacs : str) : 
    # écrire les clauses dans un fichier
    with open("input_projet.cnf",'wb') as f:
        f.write(bytes(dimacs, "UTF-8"))

def lireResultatDansFichier() :
    result = subprocess.run(
        ['gophersat-1.1.6.exe', 'input_projet.cnf'], capture_output=True, check=True, encoding="utf-8"
    )
    string = str(result.stdout)
    lines = string.splitlines()
    deuxiemeLigne = lines[1] # ligne avec SAT ou UNSAT enlever s et espace 
    deuxiemeLigne = deuxiemeLigne.replace('s ', '')
    deuxiemeLigne = deuxiemeLigne.replace('\n', '')
    troisiemeLigne = ''
    if('SATISFIABLE' == str(deuxiemeLigne)) : 
        troisiemeLigne = lines[2]
        troisiemeLigne = troisiemeLigne.replace('v', '')  # le modèle  enlever v
        troisiemeLigne = troisiemeLigne.replace('\n', '')
    return deuxiemeLigne, troisiemeLigne


def jouerCarte() :
    statut = 'OK' # relier avec la fonction coucou de l'API 

    while(statut == 'OK') : # pour une carte donc : and statut != "GG" and statut != 'KO'
        # faire une grande boucle tant que la grille n'est pas perdu 
        clauses = constructionClauses()
        if(len(clauses) == 0) : # s'il ny a pas de codes 
            for i in range(longueur):
                for j in range(largeur): 
                    if(grille[i][j]['etat'] == -1) : 
                        statut = decouvrir(i,j)
                        continue
        dimacs = clauses_to_dimacs(clauses)

        # on écrit la chaine de clause dans le fichier
        ecrireClausesDansFichier(dimacs)
        appelGophersat()    

        # lire le résultat
        sat, modele = lireResultatDansFichier()
        # tant que le résultat n'est pas UNSAT ou qu'on a pas tout mis en négatif, on continue 
        # on fait le modèle opposé
        nouveauDimacs = str
        nouveauDimacs = str(modele)
        nouveauDimacs = nouveauDimacs.replace(' ', ' -')
        nouveauDimacs = nouveauDimacs.replace('--', '')
        nouveauDimacs = nouveauDimacs.replace(' -0', '')
        nouveauDimacs = nouveauDimacs + ' 0\n'
        nouveauDimacs = nouveauDimacs[1:-1]
        nn = clauses_to_dimacs2(clauses, nouveauDimacs)
        ecrireClausesDansFichier(nn)
        appelGophersat()    

        # lire le résultat
        sat, modele2 = lireResultatDansFichier()
        # si avec le modèle opposé c'est UNSAT alors c'est que tout les littéraux de l'autres modèle sont certans
        if(sat == 'UNSATISFIABLE') : 
            litteraux = modele.split() # sépare au niveau de l'esapce
            for i in range(0, len(litteraux)-1):
                litteral = int(litteraux[i])
                if(litteral < 0) :
                    continue
                coordonnees = litteralACoordonnees(litteral)
                x = coordonnees[0]
                y = coordonnees[1]
                if(grille[x][y]['etat'] == -1) :
                    if(litteral > 0) : # isle littéral est positif
                        if(litteral%4 == 1) : 
                            statut = devinerAnimal(x, y, 'T') # faire une discover pour Tigre
                            if(statut != 'OK') : 
                                break
                        elif(litteral%4 == 2) : 
                            statut = devinerAnimal(x, y, 'S') # faire une discover pour Requin
                            if(statut != 'OK') : 
                                break
                        elif(litteral%4 == 3) : 
                            statut = devinerAnimal(x, y, 'C') # faire une discover pour Crocodile
                            if(statut != 'OK') : 
                                break
                        else : 
                            statut = decouvrir(x, y)
                            if(statut != 'OK') : 
                                break
            continue # on sort de du while 
        # ajouter le négatif du modèle trouvé dans la base de clause 

        else : # si ce n'est pas certains, on teste tous les littéraux un par un 
            litteraux = modele.split() # sépare au niveau de l'esapce
            setNegatif = []
            setPositif = set()
            litteral = '0'
            for i in range(0, len(litteraux)-1):
                litteral = int(litteraux[i])
                nouveauDimacs = str
                nouveauDimacs = str(-litteral) + ' 0' 
                nn = clauses_to_dimacs2(clauses, nouveauDimacs)
    
                ecrireClausesDansFichier(nn)
                appelGophersat() 
                sat, modele2 = lireResultatDansFichier()
                
                if(sat == 'UNSATISFIABLE') : 
                    if(litteral > 0) :
                        setPositif.add(litteral)                    
                    elif(litteral < 0 and len(setPositif) == 0) : #le setNegatif est intéressant uniquement si aucun positif sûr n'a été trouvé
                        if(litteral < 0) :
                            litteral = -litteral
                        mod = nbEtats
                        if(len(setNegatif) > 0) : 
                            mod = setNegatif[-1]%4
                            mod = nbEtats - mod
                            if(mod == 4) : 
                                mod = 0
                        # s'il y en a déjà deux et qu'on peut en ajouter un troisème ce nest pas bon 
                        if(len(setNegatif) == 0) : # si la liste est vide on ajoute le littéral 
                            setNegatif.append(litteral)
                        # le dernier négatif mis dans la liste. On ajoute le négatif actuel s'il désigne la même case 
                        elif((setNegatif[-1] + mod) >= litteral) : # vérifier qu'ils font parti du même groupe 
                            # faire un set 
                            setNegatif.append(litteral)
                        elif((setNegatif[-1] + mod) < litteral and len(setNegatif) != 2) :
                            setNegatif.clear()
                            setNegatif.append(litteral)

            # si on est sorti de la boucle est que rien n'est sûr, il faut étblir une stratégie 
            
            if(len(setPositif) == 0) : 
                requin = True
                libre = True
                tigre = True
                croco = True
                x = 0
                y = 0
                if(len(setNegatif) > 0):

                    # cas où on n'a rien trouvé de certain
                    # si on a des négatifs sûrs on apporte des précisions
                    for negatif in setNegatif : 
                        if(negatif < 0) :
                            negatif = -negatif
                        coordonnees = litteralACoordonnees(negatif)
                        x = coordonnees[0]
                        y = coordonnees[1]
                        if(negatif%4 == 1) :
                            tigre = False
                        elif(negatif%4 == 2) :
                            requin = False
                        elif(negatif%4 == 3) :
                            croco = False
                        else :
                            libre = False
                else : 
                    m = Tuple
                    if(len(bordure) > 0) :
                        for e in bordure : 
                            m = e
                            break
                        voisins = voisinage(m[0], m[1])
                        for voisin in voisins : 
                            x = voisin[0]
                            y = voisin[1]
                            if(grille[x][y]['etat'] == -1) : 
                                break

                # pour les coordonnées qu'on a on pourrait recherche tous les voisins qui sont dans la bordure 
                # et recupérer les nombres d'animaux et on fait une moyenne ?
                # sinon on se base sur le nombre d'animaux 
                nbTigresRestants = nbTigres - nbTigresDecouverts
                nbCrocosRestants = nbCrocos - nbCrocosDecouverts
                nbRequinsRestants = nbRequins - nbRequinsDecouverts
                if(libre == True) : 
                    statut = decouvrir(x, y)
                    if(statut != 'OK') : 
                        break
                elif(tigre == True and requin == True and croco == True) : 
                    if(nbTigresRestants >= nbCrocosRestants and nbTigresRestants >= nbRequinsRestants) :
                        statut = devinerAnimal(x, y, 'T') # faire une discover pour Tigre
                        if(statut != 'OK') : 
                            break
                    elif(nbCrocosRestants >= nbTigresRestants and nbCrocosRestants >= nbRequinsRestants) :
                        statut = devinerAnimal(x, y, 'C') # faire une discover pour Crocodile
                        if(statut != 'OK') : 
                            break
                    elif(nbRequinsRestants >= nbTigresRestants and nbRequinsRestants >= nbCrocosRestants) :
                        statut = devinerAnimal(x, y, 'S') # faire une discover pour Requin
                        if(statut != 'OK') : 
                            break
                elif(tigre == True and requin == True) :
                    if(nbTigresRestants >= nbRequinsRestants) :
                        statut = devinerAnimal(x, y, 'T') # faire une discover pour Tigre
                        if(statut != 'OK') : 
                            break
                    else :
                        statut = devinerAnimal(x, y, 'S') # faire une discover pour Requin
                        if(statut != 'OK') : 
                            break
                elif(tigre == True and croco == True) :
                    if(nbTigresRestants >= nbCrocosRestants) :
                        statut = devinerAnimal(x, y, 'T') # faire une discover pour Tigre
                        if(statut != 'OK') : 
                            break
                    else :
                        statut = devinerAnimal(x, y, 'C') # faire une discover pour Croco
                        if(statut != 'OK') : 
                            break
                elif(croco == True and requin == True) :
                    if(nbCrocosRestants >= nbRequinsRestants) :
                        statut = devinerAnimal(x, y, 'C') # faire une discover pour Croco
                        if(statut != 'OK') : 
                            break
                    else :
                        statut = devinerAnimal(x, y, 'S') # faire une discover pour Requin
                        if(statut != 'OK') : 
                            break
                elif (tigre == True) :
                    statut = devinerAnimal(x, y, 'T') # faire une discover pour Tigre
                    if(statut != 'OK') : 
                        break
                elif(requin == True) :
                    statut = devinerAnimal(x, y, 'S') # faire une discover pour Requin
                    if(statut != 'OK') : 
                        break
                elif(croco == True) : 
                    statut = devinerAnimal(x, y, 'C') # faire une discover pour Crocodile
                    if(statut != 'OK') : 
                        break

            else : # si on a des positifs surs
                for positif in setPositif : 
                    # si on est sûr d'un truc 
                    if(positif < 0) : 
                        positif = -positif
                    coordonnees = litteralACoordonnees(positif)
                    x = coordonnees[0]
                    y = coordonnees[1]
                    if(grille[x][y]['etat'] == -1) :
                        if(positif%4 == 1) : 
                            statut = devinerAnimal(x, y, 'T') # faire une discover pour Tigre
                            if(statut != 'OK') : 
                                break
                        elif(positif%4 == 2) : 
                            statut = devinerAnimal(x, y, 'S') # faire une discover pour Requin
                            if(statut != 'OK') : 
                                break
                        elif(positif%4 == 3) : 
                            statut = devinerAnimal(x, y, 'C') # faire une discover pour Crocodile
                            if(statut != 'OK') : 
                                break
                        else : 
                            statut = decouvrir(x, y)
                            if(statut != 'OK') : 
                                break

def main() : 
    while(1):
        try :
            bordure.clear()
            statut = initialisation() 
            if(statut == 'Err') : 
                break
            if(len(bordure) == 0) : 
                statut = decouvrir(depart[0], depart[1])
                if(statut != 'OK') : 
                    continue
                #croco.discover(x, y)
            # si info est vide il faut faire un discover 
            jouerCarte()
        except :
            print("Erreur code")
        
main()
